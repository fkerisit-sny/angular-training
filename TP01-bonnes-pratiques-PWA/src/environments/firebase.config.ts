// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "",
  authDomain: "ng-training-fjjk.firebaseapp.com",
  projectId: "ng-training-fjjk",
  storageBucket: "ng-training-fjjk.appspot.com",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};
