import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable, from, fromEvent, interval, Subscription } from 'rxjs';
import { first, last, filter, mapTo, scan, take } from 'rxjs/operators';
@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.scss'],
})
export class OperatorsComponent implements OnInit {
  // props / states
  public title: string = 'La programmation RxJS';
  @ViewChild('formations') elementUl!: ElementRef;
  // TS strict mode
  // ? optional
  // ! mendatory

  @ViewChild('jaime') jaime!: ElementRef;
  @ViewChild('jaimePas') jaimePas!: ElementRef;
  @ViewChild('nombreJaime') nombreJaime!: ElementRef;
  @ViewChild('nombreJaimePas') nombreJaimePas!: ElementRef;

  public tempsSecondes: number;
  public subscriptionCount: Subscription;
  public flag: boolean;

  constructor() {
    //init
    this.tempsSecondes = 0;
    this.subscriptionCount = new Subscription();
    this.flag = true;
  }

  ngOnInit(): void {}

  ngAfterViewInit() {
    fromEvent(this.jaime.nativeElement, 'click')
      .pipe(
        mapTo(1),
        scan((total: number, valeur: number) => {
          return total + valeur;
        })
      )
      .subscribe((nbJaime: number) => {
        this.nombreJaime.nativeElement.innerHTML = `${nbJaime} J'aime(s)`;
      });
  }

  //méthodes
  public createObservable1 = () => {
    const elementUl: HTMLElement = <HTMLElement>(
      document.querySelector('ul#formations')
    );
    elementUl.innerHTML = '';

    const formations: string[] = ['NG11', 'React16', 'Vue', 'TypeScript', 'ES'];
    console.table(formations);

    // // Création d'un observable à partir de 'from'
    // const formations$: Observable<string> = from(formations);
    // // Abonnement
    // formations$.subscribe(
    //   (formation) => {
    //     console.log(formation);
    //   }
    // )

    // from(formations).subscribe(
    //   (formation) => {
    //     const li: HTMLElement = document.createElement('li');
    //     li.innerHTML=formation;
    //     elementUl.appendChild(li);
    //   }
    // )

    from(formations)
      .pipe(
        first(
          // formation => formation === 'ES' // forme permissive ES6
          (formation) => {
            return formation === 'ES';
          }
        )
        // last()
      )
      .subscribe((formation) => {
        const li: HTMLElement = document.createElement('li');
        li.innerHTML = formation;
        elementUl.appendChild(li);
      });
  };

  public createObservable2 = () => {
    const formations: any[] = [
      {
        cours: 'NG11',
        duree: 4,
      },
      {
        cours: 'React',
        duree: 3,
      },
      {
        cours: 'Vue',
        duree: 2,
      },
      {
        cours: 'ECMA script',
        duree: 2,
      },
      {
        cours: 'TypeScript',
        duree: 2,
      },
    ];

    this.elementUl.nativeElement.innerHTML = '';

    from(formations)
      .pipe(
        filter((formation) => {
          return formation.duree === 2;
        })
      )
      .subscribe((formation) => {
        const li: HTMLElement = document.createElement('li');
        li.innerHTML = formation.cours + ' (durée: ' + formation.duree + ')';
        this.elementUl.nativeElement.appendChild(li);
      });
  };

  public toggleStartStop = () => {
    console.log('flag', this.flag);
    if (this.flag) {
      const compteur$: Observable<number> = interval(1000).pipe(
        take(20) //nombre de répétitions
      );

      this.subscriptionCount = compteur$.subscribe((valeurInterval: number) => {
        this.tempsSecondes = valeurInterval;
      });
      this.flag = false;
    } else {
      this.subscriptionCount.unsubscribe();
      this.flag = true;
      this.tempsSecondes = 0;
    }
  };
}
