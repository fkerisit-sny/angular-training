import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListeDesFilmsComponent } from './liste-des-films/liste-des-films.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';

import { firebaseConfig } from '../../../../environments/firebase.config';

@NgModule({
  declarations: [ListeDesFilmsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    // la conf de firebase est lu depuis le fichier environnement
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  exports: [ListeDesFilmsComponent],
})
export class FilmsModule {}
