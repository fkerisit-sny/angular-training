import { Component, OnInit, OnDestroy } from '@angular/core';

import { FilmsService } from '../films.service';
import { Subscription } from 'rxjs';
// ----------------------------------------------
import { Films } from '../films';
import { PanierService } from 'src/app/webApp/films/panier.service';

@Component({
  selector: 'app-liste-des-films',
  templateUrl: './liste-des-films.component.html',
  styleUrls: ['./liste-des-films.component.scss'],
})
export class ListeDesFilmsComponent implements OnInit, OnDestroy {
  // props ou states
  public stateFilms: Array<Films>; //  strict mode NG >=11
  public stateSubscription: Subscription;
  public panierFilm: Films[];
  public commandesFilm: any[];

  // constr

  constructor(
    private _filmsService: FilmsService,
    private _panierService: PanierService
  ) {
    // initialiser les States ou props
    this.stateFilms = []; //  strict mode NG >=11
    this.stateSubscription = new Subscription();
    this.panierFilm = [];
    this.commandesFilm = [];
  }

  // lifeCycle
  ngOnInit(): void {
    console.log(
      '◄◄ Le service renvoie un Observable ',
      this._filmsService.getFilms()
    );

    this.stateSubscription = this._filmsService.getFilms().subscribe(
      // abonnement à l'observable
      (datas: Array<Films>) => {
        this.stateFilms = datas;
      }
    );

    // récupération des commandes
    this._panierService.getCommandesFilms().subscribe(
      (data) => {
        console.table(data);
        this.commandesFilm = data;
      }
    );
  }

  ngOnDestroy(): void {
    this.stateSubscription.unsubscribe();
    // désabonnement
    // window.alert('Comp DESTROY');
  }

  // Méthodes
  public addPanierFilm = (film: Films) => {
    if (!this.panierFilm.includes(film)) {
      this.panierFilm.push(film);
      console.table(this.panierFilm);
    }
  };

  public supprPanierFilm = (film: Films) => {
    let keyPanierFilm = this.panierFilm.indexOf(film);
    if (keyPanierFilm > -1) this.panierFilm.splice(keyPanierFilm, 1);
  };

  // -------------------------------
  public afficheBtnPanier() {
    if (this.panierFilm.length === 0) {
      return 'none';
    } else {
      return 'block';
    }
  }

  public validerPanier = () => {
    console.clear();
    console.log({...this.panierFilm}); //spread operator ES6

    let data = {...this.panierFilm};

    this._panierService.addPanierToFirebase(data).then(
      (res) => {
        console.log(res);
      }
    )
  };

  public supprCommande = (commande: any) => {
    this._panierService.supprCommandeFilm(commande.payload.doc.id);
  }
}
