import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// ----------------------------------------------------
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
// ----------------------------------------------------
import { Films } from './films';

@Injectable({
  providedIn: 'root',
})
export class FilmsService {
  // 1- Props ou states

  // 2- Constructor
  constructor(private _http: HttpClient) {}

  // 3- Méthodes
  public getFilms(): Observable<Films[]> {
    return this._http.get<Films[]>('http://localhost:3001/films').pipe(
      // pipe permet d'enchainer plusieurs opérateurs RXJS
      tap(
        // opérateur d'observation
        (responseHTTP) => {
          console.log('►► Service Réponse HTTP : ', responseHTTP);
        }
      ),
      map(
        // opérateur de transformation
        (datas: Films[]) => {
          return datas.filter((data: Films) => {
            console.log(data);

            return data.id >= 1;
          });
        }
      )
    );
  }
}
