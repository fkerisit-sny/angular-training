import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AngularComponent } from './children-nav/angular/angular.component';
import { ReactComponent } from './children-nav/react/react.component';
import { VueComponent } from './children-nav/vue/vue.component';

export const routesFW: Routes = [
  {
    path: 'angular-11',
    component: AngularComponent,
    outlet: 'outletFW',
  },
  {
    path: 'react',
    component: ReactComponent,
    outlet: 'outletFW',
  },
  {
    path: 'vue',
    component: VueComponent,
    outlet: 'outletFW',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routesFW)],
  exports: [RouterModule],
})
export class FrameworksRoutingModule {}
