import { NgModule } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './webApp/accueil/home/home.component';
import { FormGroupComponent } from './webApp/forms-reactives/form-group/form-group.component';
import { LandingFwComponent } from './webApp/frameworks/landing-fw/landing-fw.component';
import { OperatorsComponent } from './webApp/rxjs/operators/operators.component';
import { ListeDesFilmsComponent } from './webApp/webApp/films/liste-des-films/liste-des-films.component';

// import des routes du module enfant
// méthode qui charge tous les composants associés aux routes ==> Coûteux !
// import { routesFW } from './webApp/frameworks/frameworks-routing.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'accueil',
    component: HomeComponent,
  },
  {
    path: 'les-films',
    component: ListeDesFilmsComponent,
  },
  {
    path: 'programmation-rxjs',
    component: OperatorsComponent,
  },

  // en mode asychrone
  {
    path: 'liste-des-frameworks',
    component: LandingFwComponent,
    loadChildren: async () =>
      (await import('./webApp/frameworks/frameworks.module')).FrameworksModule,
  },
  {
    path:'form-group',
    component: FormGroupComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
