# Formation Angular, développement avancé
Cette formation Angular vous permettra de maîtriser en profondeur les bonnes pratiques de développement des applications JavaScript côté client et de librairies de composants avec les dernières version du framework Angular et le moteur de rendu optimisé Ivy.

### Objectifs pédagogiques
- [ ] Savoir utiliser les décorateurs Angular
- [ ] Architecturer les applications web complexes
- [ ] Intégrer les outils de documentation et les tests unitaires
- [ ] Développer et intégrer des librairies de composants
### Méthodes pédagogiques
Chaque nouveau concept théorique sera appliqué immédiatement et de façon pratique.

## PROGRAMME DE FORMATION
### Angular, mise en œuvre des bonnes pratiques
- [ ] Injection de dépendances.
- [ ] Types applicatifs partagés.
- [ ] PWA : les services workers.
- [ ] Requêtes HTTP avancées.
- [ ] Lazy loading.
- [ ] Automatiser la documentation.
- [ ] L'internationalisation.
### Travaux pratiques
Analyser et optimiser une application.

### Fonctionnement interne d'Angular
- [ ] ZoneJS : le concept.
- [ ] Optimisation des cycles de rendu, exécution hors ZoneJS.
- [ ] Choisir RxJS.
- [ ] Angular 9 Ivy Engine : configuration et migration.
- [ ] Utilisation des observables.
- [ ] Création, combinaison, opérateurs clés.
- [ ] Compilation ahead of time.
- [ ] Webpack bundle analyzer.
### Travaux pratiques
- [ ] Créer une application utilisant RxJS.

## Création de composants distribuables
- [ ] Les web components.
- [ ] Méthodologie : interactive component sheet.
- [ ] Les décorateurs.
- [ ] Le change detection mode.
- [ ] Composants neutres versus à état. Communication entre composants, optimisation ES6.
- [ ] Projection de contenu, pilotage de composants enfants.
- [ ] Préparer les composants pour la distribution.
- [ ] Documentation : génération dynamique.
### Travaux pratiques
Développer et packager des composants distribuables.

## Composants riches et librairies externes
- [ ] Gestion des développements multi-projet.
- [ ] Découverte de l'écosystème Angular pour la gestion des données, AngularFire, NgRx.
- [ ] Les bibliothèques UI : Angular Material, Prime NG...
- [ ] Les animations.
### Travaux pratiques
- [ ] Réutilisation de composants, intégration de librairies externes.

## Formulaire dynamique : le FormBuilder
- [ ] Création de formulaire dynamique : ReactiveFormsModule.
- [ ] FormControl et FormGroup, AbstractControl, FormArray.
- [ ] Validation et gestion d'erreur personnalisée.
- [ ] Création de modèles de données.
- [ ] Utilisation du FormBuilder.
- [ ] Création dynamique de template.
- [ ] Abstraction de composant métier de formulaire.
### Travaux pratiques
Mise en œuvre de la génération et les cycles de validation avancée de formulaire.

## Tests unitaires. Bonnes pratiques et outils.
- [ ] Karma et Jasmine.
- [ ] Tests d'intégration avec Protractor.
- [ ] Le Code-Coverage.
- [ ] Behaviour driven development, Test driven development.
- [ ] Cas de test : pipe, component, service, etc.
## Travaux pratiques
- [ ] TDD : développer une application à partir de tests unitaires.

---
https://www.orsys.fr/formation/ANY